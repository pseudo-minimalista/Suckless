#ifndef _length_new_random_h
#define _length_new_random_h

#include "length_new_random.c"

int
length_new_random(int length_min,
		  int length_max);

#endif
