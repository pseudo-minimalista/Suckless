/*  This file is part of smatrix.                                           */
/*                                                                          */
/*  Smatrix is free software: you can redistribute it and/or modify it      */
/*  under the terms of the GNU General Public License as published by       */
/*  the Free Software Foundation, either version 2 of the License, or       */
/*  (at your option) any later version.                                     */
/*                                                                          */
/*  Smatrix is distributed in the hope that it will be useful, but WITHOUT  */
/*  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY      */
/*  or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public         */
/*  License for more details.                                               */
/*                                                                          */
/*  You should have received a copy of the GNU General Public License       */
/*  along with smatrix. If not, see <https://www.gnu.org/licenses/>.        */

#include <stdio.h>
#include </usr/include/stdlib.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include "/usr/include/signal.h"
#include <time.h>
#include <argp.h>

#include "matrix.h"
#include "drop.h"
#include "settings.h"
#include "slist.h"
#include "argp_options.h"

#include "event_handler.h"
#include "slist_new.h"
#include "char_new_random.h"
#include "advance.h"
#include "check_size.h"
#include "draw.h"
#include "drop_new_random.h"
#include "matrix_new.h"
#include "position_new_random.h"
#include "spawn.h"
#include "velocity_new_random.h"

int
main(int argc, char *argv[]);

